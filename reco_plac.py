import cv2
import pytesseract

pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'

placa =[];
#imagen para prueba
imagen = cv2.imread('test_imagees/5.jpeg')
#Transforma imagen original a imagen en blanco y negro
bn = cv2.cvtColor(imagen, cv2.COLOR_BGR2GRAY)
#se agrega un blur para que se descarten elementos no requeridos 
bn = cv2.bilateralFilter(bn, 11, 17, 17)
#Se genera un umbral para reconocer los bordes 
canny = cv2.Canny(bn,150,200)
#Se agrega la siguinete funcion para cerrar los bordes de la placa 
canny = cv2.dilate(canny, None, iterations=1)
#Empezamos a dibujar contornos e indicamos la funcion para encontrar el contorno correcto 
cnts,_ = cv2.findContours(canny.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
cnts = sorted(cnts, key=cv2.contourArea, reverse=True)[:30]

#Declaramos el contorno de la placa y las cordenadas vacias
contorno_placa = None
placa = None
x= None
y= None
w= None
h= None

#utilizamos una funcion for para ir reccorriendo cada contorno y asi solo extraer el contorno de la placa 
for c in cnts:
    #encontrar el perimetro del contorno, debe ser 
    perimetro = cv2.arcLength(c, True)
    aprox = cv2.approxPolyDP(c, 0.02*perimetro, True)
    #Esta funcion revisa si es un rectangulo
    if len(aprox)==4:
        contorno_placa = aprox
        x, y ,w ,h = cv2.boundingRect(c)
        placa = bn[y:y + h, x:x + w]
        break
###############ESTA SECCION ES PARA VER LOS PASOS DE LAS IMAGENES Y MEJORARLAS############
#cv2.imshow('Image', placa)
#cv2.imshow('Canny', canny)
#cv2.moveWindow('Image', 45,10)
#cv2.waitKey(0)
############################################################
(thresh, bn) =  cv2.threshold(placa,80,255, cv2.THRESH_BINARY)
#Reconocimiento de texto 
text =  pytesseract.image_to_string(bn, config= '--psm 6')

#Se dibuja la placa y se escribe el texto de la misma 
imagen= cv2.rectangle(imagen,(x, y), (x+w, y+h), (0,255,0), 3)
imagen= cv2.putText(imagen, text.upper(), (x+50, y-10), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2, cv2.LINE_AA)

print("DETECCION DE PLACA", text.upper())

cv2.imshow("Deteccion de placas", imagen)
cv2.waitKey(0)